FILE=centos

all: $(FILE).tex
	xelatex -shell-escape $(FILE).tex
	xelatex -shell-escape $(FILE).tex

clean:
	rm -rf $(FILE).aux $(FILE).bcf $(FILE).log $(FILE).out $(FILE).run.xml
	rm -rf $(FILE).bbl $(FILE).blg texput.log _minted-$(FILE)
	rm -rf $(FILE).toc $(FILE).nav $(FILE).snm $(FILE).vrb
